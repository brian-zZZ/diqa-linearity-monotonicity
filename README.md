# DIQA-linearity-monotonicity

The source codes for the paper "Camera Captured DIQA with Linearity and Monotonicity Constraints".

The models are trained and evaluated on SOC dataset, which can be downloaded from: [baidu pan](https://pan.baidu.com/s/1SwRe8wCU_b3p0oQc4Pgdng) with passcode: uyl6

