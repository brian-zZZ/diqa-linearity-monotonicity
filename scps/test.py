import os, sys, csv
import re
import cv2
import argparse
import random
import tensorflow as tf
import keras
from keras import backend as K
import numpy as np
from scipy import stats
from nets import SiameseDIQAModel


def _run_diqa_mask(blur_image, mask_image, patch_size, sample_num, model=None):
    def select_patch(blur_image, mask_image, sample_num):
        X = []
        nonzero_coords = np.transpose(np.nonzero(mask_image))

        for idx in random.sample(range(0, nonzero_coords.shape[0]), sample_num * 2):
            x, y = nonzero_coords[idx][1], nonzero_coords[idx][0]
            x = x - patch_size if x > blur_image.shape[1] - patch_size else x
            y = y - patch_size if y > blur_image.shape[0] - patch_size else y
            oim = blur_image[y : y + patch_size, x : x + patch_size]
            X.append(oim.tolist())

        return X

    X = select_patch(blur_image, mask_image, sample_num)
    X = np.reshape(X, (len(X), patch_size, patch_size, 1)).astype("float32") / 255.0

    scores = model.predict([X[::2], X[1::2]], batch_size=1, verbose=0)

    return np.mean(np.squeeze(scores))


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    parser = argparse.ArgumentParser(description="Document quality estimation.")
    parser.add_argument("-i", "--input-csv", dest="input_db", help="The input dataset")
    parser.add_argument(
        "-s",
        "--image-size",
        type=int,
        dest="patch_size",
        help="The cropped image patch size",
    )
    parser.add_argument(
        "-n",
        "--sample-num",
        type=int,
        dest="sample_num",
        help="The sample number for each test image",
    )
    parser.add_argument(
        "-m", "--model-file", dest="model", help="The pretrained CNN quality model"
    )
    args = parser.parse_args()

    # using CPU
    config = tf.ConfigProto(device_count={"GPU": 1})
    sess = tf.Session(config=config)
    K.set_session(sess)

    # create network and copy weights from model
    if os.path.isfile(args.model):
        model = SiameseDIQAModel().build_model(input_size=args.patch_size)
        model.load_weights(args.model)
    else:
        raise IOError(f"Load pre-trained model {args.model} failed.")

    predicts, groundtruths = [], []
    with open(args.input_db, "r") as fh:
        # load input images
        reader = csv.reader(fh)
        for row in reader:
            blur_image_path = row[0]
            mask_image_path = row[2]

            blur_image = cv2.imread(blur_image_path, cv2.IMREAD_GRAYSCALE)
            mask_image = cv2.imread(mask_image_path, cv2.IMREAD_GRAYSCALE)

            scores = []
            for i in range(10):
                score = _run_diqa_mask(
                    blur_image,
                    mask_image,
                    args.patch_size,
                    args.sample_num,
                    model=model,
                )
                scores.append(score)
            score_mean = np.mean(scores)
            print(f"Done: {row[0]} {score_mean} {row[3]}")

            predicts.append(score_mean)
            groundtruths.append(float(row[3]))

    cor, pval = stats.spearmanr(groundtruths, predicts)
    print(f"Spearman correlation: {cor}, p-val: {pval}")

    cor, pval = stats.pearsonr(groundtruths, predicts)
    print(f"Pearson correlation: {cor}, p-val: {pval}")


if __name__ == "__main__":
    sys.exit(main())
