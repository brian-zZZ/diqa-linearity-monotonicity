import warnings

with warnings.catch_warnings():
    import cv2
    import csv
    import scipy
    import random
    import numpy as np
    from collections import Counter
    from sklearn.utils import shuffle
    from keras.utils import Sequence


class DIQASequenceMask(Sequence):
    def __init__(self, input_db, patch_size, batch_size, sample_num):
        self.db = self._load_db(input_db)
        self.patch_size = patch_size
        self.batch_size = batch_size
        self.sample_num = sample_num
        self.on_epoch_end()

    def _load_db(self, db):
        """Load database into memory
           db: dataset csv file (Format: original_image,normalized_image,image_mask,ocr_score)
        """
        image_pair = {}
        with open(db, "r") as fh:
            reader = csv.reader(fh)
            for row in reader:
                image_pair[row[0]] = (row[2], float(row[3]))

        return image_pair

    def _prepare_data(self, data):
        def select_patch(blur_image_path, mask_image_path, sample_num):
            data = []
            blur_image = cv2.imread(blur_image_path, cv2.IMREAD_GRAYSCALE)
            mask_image = cv2.imread(mask_image_path, cv2.IMREAD_GRAYSCALE)

            nonzero_coords = np.transpose(np.nonzero(mask_image))

            for idx in random.sample(range(0, nonzero_coords.shape[0]), sample_num * 2):
                x, y = nonzero_coords[idx][1], nonzero_coords[idx][0]
                x = (
                    x - self.patch_size
                    if x > blur_image.shape[1] - self.patch_size
                    else x
                )
                y = (
                    y - self.patch_size
                    if y > blur_image.shape[0] - self.patch_size
                    else y
                )
                oim = blur_image[y : y + self.patch_size, x : x + self.patch_size]
                data.append(oim.tolist())

            return data[::2], data[1::2]

        X1, X2, Y = [], [], []
        samples = np.random.choice(list(data.keys()), self.sample_num)
        counts = Counter(samples)

        for blur_image_path in counts:
            mask_image_path = data[blur_image_path][0]
            x1, x2 = select_patch(
                blur_image_path, mask_image_path, counts[blur_image_path]
            )
            X1 += x1
            X2 += x2
            Y += [data[blur_image_path][1]] * counts[blur_image_path]

        X1 = (
            np.reshape(X1, (len(X1), self.patch_size, self.patch_size, 1)).astype(
                "float32"
            )
            / 255.0
        )
        X2 = (
            np.reshape(X2, (len(X2), self.patch_size, self.patch_size, 1)).astype(
                "float32"
            )
            / 255.0
        )

        return shuffle(X1, X2, Y)

    def on_epoch_end(self):
        self.x1, self.x2, self.y = self._prepare_data(self.db)

    def __len__(self):
        return int(np.ceil(self.sample_num) / float(self.batch_size))

    def __getitem__(self, idx):
        batch_x1 = self.x1[idx * self.batch_size : (idx + 1) * self.batch_size]
        batch_x2 = self.x2[idx * self.batch_size : (idx + 1) * self.batch_size]
        batch_y = self.y[idx * self.batch_size : (idx + 1) * self.batch_size]

        return [batch_x1, batch_x2], batch_y
