#!/bin/bash

# expt's output dirs
model_dir=`pwd`/expts/models
data_dir=`pwd`/expts/data
tmp_dir=`pwd`/expts/.tmp

mkdir -p $model_dir $data_dir $tmp_dir

image_size=128
pairwise_weight=0.3

# ------------------ CNN based image quality classification training using MSE Loss ----------------------
train_base=0
if [ $train_base -eq 1 ]; then
    echo 'Start CNN quality training...'

    # input params
    train_db=$data_dir/trn.csv
    val_db=$data_dir/val.csv
    learning_rate=0.001
    batch_size=36
    sample_num=2000
    epochs=300

    # output params
    input_model_path=$model_dir/quality.base.model.${image_size}.h5
    output_model_path=$model_dir/quality.base.model.${image_size}.h5

    python -u scps/train.py --training-db $train_db --validation-db $val_db \
                            --image-size $image_size --batch-size $batch_size --sample-num $sample_num \
                            --learning-rate $learning_rate --epochs $epochs \
                            --input-model-path $input_model_path --output-model-path $output_model_path || exit 1; 
fi

# ------------------------ test on val ---------------------------
test_base=0
if [ $test_base -eq 1 ]; then
    echo 'Start CNN quality test...'

    test_db=$data_dir/val.csv
    sample_num=500
    input_model_path=$model_dir/quality.base.model.${image_size}.h5

    python -u scps/test.py -i $test_db -s $image_size -n $sample_num -m $input_model_path
fi

# --------------------------- Using Ranking Loss ------------------
train_loss=1
if [ $train_loss -eq 1 ]; then
    echo 'Start CNN quality training...'

    # input params
    train_db=$data_dir/trn.csv
    val_db=$data_dir/val.csv
    learning_rate=0.001
    batch_size=36
    sample_num=2000
    epochs=800

    # output params
    input_model_path=$model_dir/quality.loss.model.${pairwise_weight}.h5
    output_model_path=$model_dir/quality.loss.model.${pairwise_weight}.h5

    python -u scps/train.py --training-db $train_db --validation-db $val_db \
                            --image-size $image_size --batch-size $batch_size --sample-num $sample_num \
                            --learning-rate $learning_rate --epochs $epochs \
                            --pairwise-loss-weight $pairwise_weight \
                            --input-model-path $input_model_path --output-model-path $output_model_path || exit 1; 
fi

# ------------------------ test on val ---------------------------
test_loss=1
if [ $test_loss -eq 1 ]; then
    echo 'Start CNN quality test...'

    test_db=$data_dir/val.csv
    sample_num=500
    input_model_path=$model_dir/quality.loss.model.${pairwise_weight}.h5

    python -u scps/test.py -i $test_db -s $image_size -n $sample_num -m $input_model_path
fi

echo 'All done!'

exit 0;
